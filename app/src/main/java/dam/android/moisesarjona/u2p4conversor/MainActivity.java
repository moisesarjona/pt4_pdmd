package dam.android.moisesarjona.u2p4conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "Log-";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        Log.i(DEBUG_TAG,"onCreate");
    }

    private void setUI() {
        EditText etPulgada = findViewById(R.id.etPulgada);
        EditText etResultado = findViewById(R.id.etResultado);
        Button buttonConvertirPulgadas = findViewById(R.id.buttonConvertirPulgadas);
        Button buttonConvertirCm = findViewById(R.id.buttonconvertirCm);
        TextView text = findViewById(R.id.text); //Ex2.2 Añado el TextView para mostrar la excepción

        buttonConvertirPulgadas.setOnClickListener(view -> {
            try {
                etResultado.setText(convertirPulgadas(etPulgada.getText().toString()));

                //Ex2.1 creando la clausula para que se lanze la excepcion
                if (Double.parseDouble(etPulgada.getText().toString()) <= 0)
                    throw new Exception("Dato no valido");
            } catch (Exception e){
                Log.e("LogsConversor", e.getMessage());
                // text.setText(e.toString());   ----> Ex2.2 No funciona al intentar cambiar el contenido del campo text del TextView
            }
        });

        //Ex1 listener para lanzar el metodo para convertir
        buttonConvertirCm.setOnClickListener(view -> {
            try {
                etResultado.setText(convertirCm(etPulgada.getText().toString()));
            } catch (Exception e){
                Log.e("LogsConversor", e.getMessage());
            }
        });
    }

    private String convertirPulgadas(String pulgadaText){
        double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;

        Log.i(DEBUG_TAG,"Pulsado boton Pulgadas"); //Ex3 añadimos el Log para el boton de pulgadas
        return String.valueOf(pulgadaValue);
    }

    //Ex1 hacer operacion inversa para convertir de centimetros a pulgadas
    private String convertirCm(String cmText){
        double pulgadaValue = Double.parseDouble(cmText) / 2.54;

        Log.i(DEBUG_TAG,"Pulsado boton Centimetros"); //Ex3 añadimos el Log para el boton de centimetros
        return String.valueOf(pulgadaValue);
    }

    //Ex3 Mensajes de log

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG,"onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG,"onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG,"onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG,"onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG,"onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(DEBUG_TAG,"onDestroy");
    }
}